<?php

Header('Content-Type: application/x-javascript');

// リファラがないときは終了
$referrer = null;
if (isset($_GET['referrer'])) {
	$referrer = trim($_GET['referrer']);
	if ($referrer == '' || $referrer == 'Hidden-Referrer') {
		die();
	}
} else {
	die();
}

require_once( dirname(__FILE__). '/class/class.access.php');

$access = new Access();
$access->accessProcess();
