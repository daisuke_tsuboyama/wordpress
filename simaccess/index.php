<?php

require_once('./class/class.admin.php');

$admin = new Admin();
$admin->measureStart();

if ($admin->action !== null) {
	$url = new URL($admin);
	switch ($admin->action) {
			
		case 'set':
			if (($ret = $admin->set()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('設定の編集が完了しました');
			$admin->redirect();
			break;
			
		case 'create_rank':
			if (($ret = $admin->createRank()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('ランキングファイルを生成しました');
			$admin->redirect();
			break;
			
		case 'delete_log':
			if (($ret = $admin->deleteLog()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('ログの削除が完了しました');
			$admin->redirect();
			break;
			
		case 'delete_oldlog':
			if (($ret = $admin->deleteOldLog()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('過去ログの削除が完了しました');
			$admin->redirect();
			break;
			
		case 'del_rank':
			if (($ret = $admin->deleteRank()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('ランキングの削除が完了しました');
			$admin->redirect();
			break;
			
		case 'add_denyurl':
			if (($ret = $url->addDenyURL()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('拒否 URL の追加が完了しました');
			$admin->redirect();
			break;

		case 'edit_denyurl':
			if (($ret = $url->editDenyURL()) !== true) {
				error($ret);
				$admin->redirect($url->getRedirect());
			}
			setMessage('拒否 URL の編集が完了しました');
			$admin->redirect();
			break;
			
		case 'del_denyurl':
			if (($ret = $url->deleteDenyURL()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('拒否 URL の削除が完了しました');
			$admin->redirect();
			break;
			
		case 'add_repurl':
			if (($ret = $url->addReplaceURL()) !== true) {
				error($ret);
				$admin->redirect();
			}
			setMessage('置換 URL の追加が完了しました');
			$admin->redirect();
			break;
			
		case 'edit_repurl':
			if (($ret = $url->editReplaceURL()) !== true) {
				error($ret);
				$admin->redirect($url->getRedirect());
			}
			setMessage('置換 URL の編集が完了しました');
			$admin->redirect();
			break;
			
		case 'del_repurl':
			if (($ret = $url->deleteReplaceURL()) !== true) {
				error($ret);
				$admin->redirect($url->getRedirect());
			}
			setMessage('置換 URL の削除が完了しました');
			$admin->redirect();
			break;
			
		default:
			break;

	}
}

switch ($admin->mode) {
				
	default:
		$admin->i('index.php');
		break;

}
