var xvo={
	changeThum:{
		nowv:0,
		firstnum:0,
		nownum:0,
		nowsrc:"",
		init:function(context){
			if(typeof(context)=="undefined")
				jQuery('img.pic_thum').mouseover(xvo.changeThum.starter).mouseout(xvo.changeThum.stoper);
			else
				jQuery('img.pic_thum',context).mouseover(xvo.changeThum.starter).mouseout(xvo.changeThum.stoper);
			},
		getter:function(CurNum){
			var thumbs=new Array(1,5,9,13,17,21,25,29);
			for(var i=0;i<thumbs.length;i++){
				if(thumbs[i]>CurNum){
					return thumbs[i];
				}
			}
			return thumbs[0];
		},
		nexter:function(idDoc){
			if(xvo.changeThum.nowv!==0&&idDoc==xvo.changeThum.nowv){
				jQuery('#tar_'+ xvo.changeThum.nowv).attr('src',xvo.changeThum.nowsrc+ xvo.changeThum.nownum+".jpg");
				var nextthumb=xvo.changeThum.getter(xvo.changeThum.nownum);
				xvo.changeThum.nownum=nextthumb;
				setTimeout("xvo.changeThum.nexter("+ xvo.changeThum.nowv+")",1000);
			}
		},
		starter:function(){

			if(xvo.changeThum.nowv===0){
				var img=jQuery(this);
				xvo.changeThum.nowv=img.attr('id').substring(4);
				var src=img.attr('src');
				src=src.substring(0,src.lastIndexOf('.'));
				var pos=src.lastIndexOf('.');
				xvo.changeThum.firstnum=parseInt(src.substring(pos+ 1));
				xvo.changeThum.nowsrc=src.substring(0,pos+ 1);
				var nextthumb=xvo.changeThum.getter(xvo.changeThum.firstnum);
				xvo.changeThum.nownum=nextthumb;
				setTimeout("xvo.changeThum.nexter("+ xvo.changeThum.nowv+")",20);
			}
		},
		stoper:function(){
			var TmpDoc=xvo.changeThum.nowv;
			xvo.changeThum.nowv=0;
			jQuery("#tar_"+ TmpDoc).attr('src',xvo.changeThum.nowsrc+ xvo.changeThum.firstnum+".jpg");
		}
	}
};

jQuery(document).ready(function(){xvo.changeThum.init();});